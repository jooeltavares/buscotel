from pagamento import Pagamento


class Reserva:
    def __init__(self, data_entrada, data_saida, tipo_pagamento, preco):
        self.data_entrada = data_entrada
        self.data_saida = data_saida
        self.pagamento = Pagamento(preco, tipo_pagamento)
        self.preco = preco
