from reserva import Reserva


class Quarto:
    def __init__(self, numero, tipo):
        self.numero = numero
        self.tipo = tipo
        self.reservas = []

    def reservar(self, data_inicio, data_fim, tipo_pagamento, preco):
        self.reservas.append(Reserva(data_inicio, data_fim, tipo_pagamento, preco))

    def ta_disponivel(self, data_entrada, data_saida):
        for reserva in self.reservas:
            if reserva.data_entrada == data_entrada and reserva.data_saida == data_saida:
                return False
        return True
