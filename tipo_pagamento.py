import abc


class TipoPagamento(abc.ABC):
    @abc.abstractmethod
    def pagar(self):
        pass
