from quarto import Quarto
from tipo_quarto_preco import TipoQuartoPreco


class Hotel:
    def __init__(self, nome, cidade, distancia):
        self.nome = nome
        self.cidade = cidade
        self.distancia = distancia
        self.quartos = []
        self.precos = []
        self.avaliacao = 0

    def construir_quarto(self, numero, tipo):
        a = list(filter(lambda x: x.numero == numero, self.quartos))
        if a:
            raise Exception('Quarto ' + numero + ' ja cadastrado')
        self.quartos.append(Quarto(numero, tipo))

    def reservar(self, tipo, data_entrada, data_saida, tipo_pagamento):
        for quarto in self.quartos:
            if quarto.tipo != tipo or not quarto.ta_disponivel(data_entrada, data_saida):
                continue
            quarto.reservar(data_entrada, data_saida, tipo_pagamento, self.get_quarto_preco(tipo))
            return True
        return False

    def configurar_preco(self, tipo, preco):
        a = list(filter(lambda x: x.tipo == tipo, self.precos))
        if not a:
            self.precos.append(TipoQuartoPreco(tipo, preco))
        else:
            self.precos[0].preco = preco

    def get_quarto_preco(self, tipo):
        a = list(filter(lambda x: x.tipo == tipo, self.precos))
        if not a:
            raise Exception('Quarto ' + tipo.nome + ' nao econtrado')
        return a[0].preco

    def tem_quarto_tipo(self, tipo):
        for quarto_tipo in self.precos:
            if quarto_tipo.tipo == tipo:
                return True
        return False

    def tem_quarto_disponivel(self, tipo, data_entrada, data_saida):
        for quarto in self.quartos:
            if quarto.tipo == tipo and quarto.ta_disponivel(data_entrada, data_saida):
                return True
        return False

    def ta_na_faixa_preco(self, tipo, preco):
        return preco >= self.get_quarto_preco(tipo)

    def eh_na_cidade(self, cidade):
        return self.cidade == cidade
