class ListaHoteis:
    def __init__(self, tipo, ordenar):
        self.hoteis = []
        self.tipo = tipo
        self.strategy = ordenar

    def adicionar(self, hotel):
        self.hoteis.append(hotel)

    def ordenar(self):
        return self.strategy.ordenar(self)
