from ordenar import Ordenar


class OrdenarDistancia(Ordenar):
    def ordenar(self, lista_hoteis):
        return sorted(lista_hoteis.hoteis, key=lambda hotel: hotel.distancia)
