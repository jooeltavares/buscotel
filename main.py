from buscotel import BuscOtel
from ordenar_preco import OrdenarPreco
from ordenar_distancia import OrdenarDistancia
from tipo_quarto import TipoQuarto

from pagamento_cartao import Cartao
from pagamento_boleto import Boleto

if __name__ == "__main__":
    busc_otel = BuscOtel(OrdenarDistancia())

    boleto = Boleto()
    cartao = Cartao()

    single = TipoQuarto('Single')
    duplo = TipoQuarto('Duplo')
    triplo = TipoQuarto('Triplo')
    presidencial = TipoQuarto('Presidencial')

    # Hotel Maravilha
    busc_otel.cadastrar_hotel('Maravilha', 'Jampa', 100)
    busc_otel.cadastrar_quarto_hotel('Maravilha', single, '101')
    busc_otel.cadastrar_quarto_hotel('Maravilha', single, '102')
    busc_otel.configurar_preco_quarto_hotel('Maravilha', single, 200)

    # Hotel Caravelas
    busc_otel.cadastrar_hotel('Caravelas', 'Jampa', 200)
    busc_otel.cadastrar_quarto_hotel('Caravelas', single, '101')
    busc_otel.configurar_preco_quarto_hotel('Caravelas', single, 100)

    # Hotel Pad. GRASP
    busc_otel.cadastrar_hotel('Pad. GRASP', 'Sampa', 100)
    busc_otel.cadastrar_quarto_hotel('Pad. GRASP', duplo, '101')
    busc_otel.configurar_preco_quarto_hotel('Pad. GRASP', duplo, 2000)

    # Reservas
    busc_otel.reservar('Maravilha', single, '01/01/15', '02/01/15', cartao)
    # busc_otel.reservar('Maravilha', single, '01/01/15', '02/01/15', boleto)


    # Usuario input
    cidade = 'Jampa'
    data_entrada = '01/01/15'
    data_saida = '02/01/15'
    tipo = single
    valor_max = 500

    # Buscar
    print('Hotel\t\t\tValor\t\t\tAvaliacao\tDistancia')
    for hotel in busc_otel.buscar(cidade, data_entrada, data_saida, tipo, valor_max):
        print('{}\t\t{}\t\t\t\t{}\t\t\t{}'.format(hotel.nome, hotel.get_quarto_preco(single), hotel.avaliacao,
                                                  hotel.distancia))

    print('\n\n')
    busc_otel.configurar_ordenacao(OrdenarPreco())

    print('Hotel\t\t\tValor\t\t\tAvaliacao\tDistancia')
    for hotel in busc_otel.buscar(cidade, data_entrada, data_saida, tipo, valor_max):
        print('{}\t\t{}\t\t\t\t{}\t\t\t{}'.format(hotel.nome, hotel.get_quarto_preco(single), hotel.avaliacao,
                                                  hotel.distancia))
