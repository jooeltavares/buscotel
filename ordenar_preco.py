from ordenar import Ordenar


class OrdenarPreco(Ordenar):
    def ordenar(self, lista_hoteis):
        return sorted(lista_hoteis.hoteis, key=lambda hotel: hotel.get_quarto_preco(lista_hoteis.tipo))
