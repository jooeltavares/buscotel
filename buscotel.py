from hotel import Hotel
from lista_hoteis import ListaHoteis


class BuscOtel:
    def __init__(self, ordenar):
        self.hoteis = []
        self.ordenar = ordenar

    def reservar(self, nome, tipo, data_entrada, data_saida, tipo_pagamento):
        hotel = list(filter(lambda x: x.nome == nome, self.hoteis))
        if not hotel:
            raise Exception('Hotel ' + nome + ' nao encontrado')
        if not hotel[0].reservar(tipo, data_entrada, data_saida, tipo_pagamento):
            raise Exception('Hotel ' + nome + ' nao tem quarto disponivel')

    def cadastrar_hotel(self, nome, endereco, distancia):
        hotel = list(filter(lambda x: x.nome == nome, self.hoteis))
        if hotel:
            raise Exception('Hotel ja cadastrado')
        self.hoteis.append(Hotel(nome, endereco, distancia))

    def cadastrar_quarto_hotel(self, nome, tipo, numero):
        hotel = list(filter(lambda x: x.nome == nome, self.hoteis))
        if not hotel:
            raise Exception('Hotel ' + nome + ' nao encontrado')
        hotel[0].construir_quarto(numero, tipo)

    def configurar_preco_quarto_hotel(self, nome, tipo, preco):
        hotel = list(filter(lambda x: x.nome == nome, self.hoteis))
        if not hotel:
            raise Exception('Hotel ' + nome + ' nao encontrado')
        hotel[0].configurar_preco(tipo, preco)

    def configurar_ordenacao(self, ordenar):
        self.ordenar = ordenar

    def buscar(self, cidade, data_entrada, data_saida, tipo, valor_max=0.0):
        lista_hoteis = ListaHoteis(tipo, self.ordenar)
        for hotel in self.hoteis:
            if hotel.tem_quarto_tipo(tipo) and \
                    (hotel.ta_na_faixa_preco(tipo, valor_max) or valor_max == 0.0) \
                    and hotel.tem_quarto_disponivel(tipo, data_entrada, data_saida) \
                    and hotel.eh_na_cidade(cidade):
                lista_hoteis.adicionar(hotel)
        return lista_hoteis.ordenar()

    def get_preco_quarto_tipo(self, nome, tipo):
        hotel = list(filter(lambda x: x.nome == nome, self.hoteis))
        if not hotel:
            raise Exception('Hotel ' + nome + ' nao encontrado')
        return hotel[0].get_quarto_preco(tipo)
